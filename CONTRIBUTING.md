import swapi

# ¿En cuántas películas aparecen planetas cuyo clima sea árido?
# Respuesta
for planet in swapi.get_all("planets").order_by("climate"):
    if planet.climate =='arid':
        print(planet.name, planet.climate)

# ¿Cuántos Wookies aparecen en la sexta película?
# Respuesta
for species in swapi.get_all("species").iter():
    if species.films >= 20:
        print(species.name, species.films)

# ¿Cuál es el nombre de la aeronave más grande en toda la saga?
#Respuesta
for starships in swapi.get_all("starships").iter():
    if starships.cargo_capacity>=1000000000000:
        print(starships.name)
